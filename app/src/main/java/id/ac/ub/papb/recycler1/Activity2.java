package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    private TextView tvNim;
    private TextView tvNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        String nim = getIntent().getExtras().getString("nim");
        String nama = getIntent().getExtras().getString("nama");

        tvNim = findViewById(R.id.tvNim2);
        tvNama = findViewById(R.id.tvNama2);

        tvNim.setText("Nim :" + nim);
        tvNama.setText("Nama : " + nama);
    }
}